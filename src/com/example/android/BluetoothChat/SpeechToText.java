package com.example.android.BluetoothChat;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.widget.TextView;
import android.widget.Toast;

public class SpeechToText  extends Activity {
	private final int GOOGLE_STT = 1000;		//requestCode. 구글음성인식, 내가 만든 Activity
	private ArrayList<String> mResult;									//음성인식 결과 저장할 list
	
	 //최종 결과 출력하는 텍스트 뷰
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent stt = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);			//intent 생성
		stt.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());	//음성인식을 호출한 패키지
		stt.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR");							//음성인식 언어 설정
		stt.putExtra(RecognizerIntent.EXTRA_PROMPT, "말을 하세요.");						//사용자에게 보여 줄 글자
		
		startActivityForResult(stt, GOOGLE_STT);
		
	}

	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if( resultCode == RESULT_OK  && requestCode == GOOGLE_STT){		//결과가 있으면
			showSelectDialog(requestCode, data);				//결과를 다이얼로그로 출력.
		}
		else															//결과가 없으면 에러 메시지 출력
		{	String msg = null;
			
			switch(resultCode){
				case SpeechRecognizer.ERROR_AUDIO:
					msg = "오디오 입력 중 오류가 발생했습니다.";
					break;
				case SpeechRecognizer.ERROR_CLIENT:
					msg = "단말에서 오류가 발생했습니다.";
					break;
				case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
					msg = "권한이 없습니다.";
					break;
				case SpeechRecognizer.ERROR_NETWORK:
				case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
					msg = "네트워크 오류가 발생했습니다.";
					break;
				case SpeechRecognizer.ERROR_NO_MATCH:
					msg = "일치하는 항목이 없습니다.";
					break;
				case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
					msg = "음성인식 서비스가 과부하 되었습니다.";
					break;
				case SpeechRecognizer.ERROR_SERVER:
					msg = "서버에서 오류가 발생했습니다.";
					break;
				case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
					msg = "입력이 없습니다.";
					break;
			}
			
			if(msg != null)		//오류 메시지가 null이 아니면 메시지 출력
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
		}
	}
	
	//결과 list 출력하는 다이얼로그 생성
	private void showSelectDialog(int requestCode, Intent data){
		String key = "";
		int mResultLength = 0; 
		int i=0;
		String compareData= "문 닫아";
		String mResultData2="변동업";
			//구글음성인식이면
			key = RecognizerIntent.EXTRA_RESULTS;	//키값 설정
			mResult = data.getStringArrayListExtra(key);		//인식된 데이터 list 받아옴.
			String[] result = new String[mResult.size()];			//배열생성. 다이얼로그에서 출력하기 위해
			mResult.toArray(result);		
			mResultLength = mResult.size();
			
			for(i=0;i<mResultLength;i++) {
				if(mResult.get(i).equals(compareData)) {
					mResultData2 = mResult.get(i);
				}
			}
			
			if(mResultData2 == "문 열어" ){
			}
			
			else if(mResultData2 == "문 닫아") {
			}
			else if(mResultData2 == "창문 열어") {
			}
			else if(mResultData2 == "창문 닫아") {
			}
			else if(mResultData2 == "불 꺼") {
			}
			else if(mResultData2 == "불 켜") {
			}
			else {
				mResultData2 = "다시 시도해주세요";
			}
					
	}
}