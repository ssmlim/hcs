package com.example.android.BluetoothChat;

import java.util.ArrayList;
import java.lang.String;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class BluetoothChat extends Activity {

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private final int GOOGLE_STT = 1000;	
    private ArrayList<String> mResult;
    
    // Layout Views
    private TextView mTitle;
    private TextView mSTTResultView;
    private ListView mConversationView;
    public String mShowResult = "";
	public	String mSendResultCode = "";
  	public String TX_Data="";
  	public String RX_Data="";
	private int hope_temp=25;
	
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the chat services
    private BluetoothChatService mChatService = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set up the window layout
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        // Set up the custom title
        mTitle = (TextView) findViewById(R.id.title_left_text);
        mTitle.setText(R.string.app_name);
        mTitle = (TextView) findViewById(R.id.title_right_text);
         
        ImageButton mSTTButton = (ImageButton) findViewById(R.id.button_stt);
        mSTTButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
        		Intent stt = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);			//intent 생성
        		stt.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());	//음성인식을 호출한 패키지
        		stt.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR");							//음성인식 언어 설정
        		stt.putExtra(RecognizerIntent.EXTRA_PROMPT, "말을 하세요.");						//사용자에게 보여 줄 글자
        		
        		startActivityForResult(stt, GOOGLE_STT);		
            }
        });
        
        ImageButton mBluetoothButton = (ImageButton) findViewById(R.id.button_bt);
        mBluetoothButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	Intent serverIntent = new Intent(BluetoothChat.this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
        });
        
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
     
            Toast.makeText(this, "블루투스를 이용할 수 없습니다", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the chat session
        } else {
            if (mChatService == null) setupChat();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
              // Start the Bluetooth chat services
              mChatService.start();
            }
        }
    }

    private void setupChat() {
        // Initialize the array adapter for the conversation thread
        mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
        mConversationView = (ListView) findViewById(R.id.in);
        mConversationView.setAdapter(mConversationArrayAdapter);

        // Initialize the compose field with a listener for the return key


        // Initialize the send button with a listener that for click events


        // Initialize the BluetoothChatService to perform bluetooth connections
        mChatService = new BluetoothChatService(this, mHandler);

    }
    
  //결과 list 출력하는 다이얼로그 생성
  	private void showSelectDialog(int requestCode, Intent data){
  		String key = "";
  		int mResultLength = 0; 
  		int i=0;
  		int j=0;
  		String mResultData="";
  		String CompareData[] = { "문 열어", "문 닫아", "창문 열어", "창문 닫아", "불 켜", "불 꺼"};	
  		
  			key = RecognizerIntent.EXTRA_RESULTS;	//키값 설정
  			mResult = data.getStringArrayListExtra(key);		//인식된 데이터 list 받아옴.
  			String[] result = new String[mResult.size()];			//배열생성. 다이얼로그에서 출력하기 위해
  			mResult.toArray(result);		
  			mResultLength = mResult.size();
  			
  			
  			for(i=0;i<mResultLength;i++) {
  					for(j=0;j<6;j++) {
  						if(mResult.get(i).equals(CompareData[j])) {
  							mResultData = mResult.get(i);
  						}
  				}
  			}
  			
  			if(mResultData.equals(CompareData[0])){
  				mSendResultCode = "e";
  				mShowResult = "현관열기";
  			} 
  			
  			else if(mResultData.equals(CompareData[1])){
  				mSendResultCode = "u";
  				mShowResult = "현관닫기";
  			}
  			else if(mResultData.equals(CompareData[2])){
  				mSendResultCode = "o";
  				mShowResult = "창문열기";
  			}
  			else if(mResultData.equals(CompareData[3])){
  				mSendResultCode = "c";
  				mShowResult = "창문닫기";
  			}
  			else if(mResultData.equals(CompareData[4])){
  				mSendResultCode = "n";
  				mShowResult = "전등 켜기";
  			}
  			else if(mResultData.equals(CompareData[5])){
  				mSendResultCode = "f";
  				mShowResult = "전등 끄기";
  			}
  			else {
  				mSendResultCode = "m";
  				mShowResult = "다시 시도해주세요.";
  			}		
  			
  			TXData();
  	}
  	
  	public void TXData(){
  		
  		TX_Data = mSendResultCode;
        String message = TX_Data;
        sendMessage(message);
        
        // clear
        mSendResultCode = "w";
  	}
  	 	
  	public void TX_State() {
  		
  		if(TX_Data.equals("e") || TX_Data.equals("u") || TX_Data.equals("o") || TX_Data.equals("c") || TX_Data.equals("n") || TX_Data.equals("f")){
  				mConversationArrayAdapter.add("<SYSTEM> " + mShowResult +"를 요청하였습니다." );
  			} 
  			else if(TX_Data.equals("m")){
  				mConversationArrayAdapter.add("<SYSTEM> " + mShowResult);
  			}
  			else {
  				mConversationArrayAdapter.add("<SYSTEM> 온도를 변경하였습니다.");
  			}
  	}
  	
  	public void RXData(String message) {
  		
  		RX_Data = message;
  		  		  		
  		if(RX_Data.charAt(0) == 'e'){
  			mShowResult = "현관을 열었습니다.";
  		}
  		else if (RX_Data.charAt(0) == 'u'){
  			mShowResult = "현관을 닫았습니다.";
  		}
  		else if (RX_Data.charAt(0) == 'o'){
  			mShowResult = "창문을 열었습니다.";
  		}
  		else if (RX_Data.charAt(0) == 'c'){
  			mShowResult = "창문을 닫았습니다.";
  		}
  		else if (RX_Data.charAt(0) == 'n'){
  			mShowResult = "전등을 켰습니다.";
  		}
  		else if (RX_Data.charAt(0) == 'f'){
  			mShowResult = "전등을 껐습니다.";
  		}	
  		
  		RX_State();

  	}
  	
  	public void RX_State() {
  		
  		if(TX_Data.equals("e") || TX_Data.equals("u") || TX_Data.equals("o") || TX_Data.equals("c") || TX_Data.equals("n") || TX_Data.equals("f")){
  			mConversationArrayAdapter.add(mConnectedDeviceName+":  " + mShowResult);
  		} 
  	}
  	
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (mChatService != null) mChatService.stop();
    }

    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);
        }
    }


    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                case BluetoothChatService.STATE_CONNECTED:
                    mTitle.setText(R.string.title_connected_to);
                    mTitle.append(mConnectedDeviceName);
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothChatService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothChatService.STATE_LISTEN:
                case BluetoothChatService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                String writeMessage = new String(writeBuf);
                TX_State();
                break;
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                RXData(readMessage);
                //mConversationArrayAdapter.add(mConnectedDeviceName+":  " + readMessage);
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), 
                               mConnectedDeviceName + "에 연결되었습니다" , Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Get the BLuetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                mChatService.connect(device);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a chat session
                setupChat();
            } else {
                // User did not enable Bluetooth or an error occured
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        case  GOOGLE_STT :
        	if( resultCode == RESULT_OK){		//결과가 있으면
    			showSelectDialog(requestCode, data);				//결과를 다이얼로그로 출력.
    		}
    		else															//결과가 없으면 에러 메시지 출력
    		{	String msg = null;
    			
    			switch(resultCode){
    				case SpeechRecognizer.ERROR_AUDIO:
    					msg = "오디오 입력 중 오류가 발생했습니다.";
    					break;
    				case SpeechRecognizer.ERROR_CLIENT:
    					msg = "단말에서 오류가 발생했습니다.";
    					break;
    				case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
    					msg = "권한이 없습니다.";
    					break;
    				case SpeechRecognizer.ERROR_NETWORK:
    				case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
    					msg = "네트워크 오류가 발생했습니다.";
    					break;
    				case SpeechRecognizer.ERROR_NO_MATCH:
    					msg = "일치하는 항목이 없습니다.";
    					break;
    				case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
    					msg = "음성인식 서비스가 과부하 되었습니다.";
    					break;
    				case SpeechRecognizer.ERROR_SERVER:
    					msg = "서버에서 오류가 발생했습니다.";
    					break;
    				case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
    					msg = "입력이 없습니다.";
    					break;
    			}
    			
    			if(msg != null)		//오류 메시지가 null이 아니면 메시지 출력
    				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    		}
        }
    }
}